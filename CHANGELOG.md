# Change Log

Alle erwähnenswerten Änderungen an diesem Projekt werden in dieser Datei dokumentiert.

## [1.2.0] - 2017-12-01
## Added
- Schneller Wechsel zwischen Breite und Stärke bei Flachdraht


<<<<<<< HEAD
## [1.1.9] - 2017-10-24
### Added
- Option "Flachdraht" implementiert
### Changed
- Setup-Speicherung für Flachdraht-Option angepasst
=======
## [Unreleased]
### Added
- Feature zur Eingabe und Verwendung von Flachdraht
>>>>>>> 77cf77de38163524aba70b2fe7fff561f3dce51d


## [1.0.0] - 2017-06-25
### Changed
<<<<<<< HEAD
- Anzeige "Heat flux" angepasst
- Hilfe implementiert
=======
- Letzte kosmetische Änderungen
>>>>>>> 77cf77de38163524aba70b2fe7fff561f3dce51d


## [0.9.0] - 2017-06-22
### Added
- Material-Editor zur Bearbeitung der Drahtsorten
- Anzeige des Wertes "Heat flux"


## [0.7.0] - 2017-06-21
### Changed
- Umstellung auf Konfigurationsdatei für Daten
- Änderung am I-Feld
- Umstellung auf direkte Berechnung des Wertes Ohm/m aus spezifischem Widerstand
- Umstellung von Materialtabellen auf direkte Eingabe des Durchmessers
- automatische Umrechnung mm <-> AWG


## [0.6.2] - 2017-06-17
### Changed
- TAB-Reihenfolge korrigiert
- I- und P-Feld schreibgeschützt


## [0.6.1] - 2017-06-16
### Changed
- Symbolgrafik für X 1/2 Wicklungen ausgetauscht.


## [0.6.0] - 2017-06-15
### Added
- URIP-Rechner zur Berechnung von Stromstärke und Leistung


## [0.5.0] - 2017-06-14
### Added
- Daten für Kanthal D
- Daten für Titan
- Daten für Nickel


## [0.3.1] - 2017-06-13
### Added
- Speicherfunktion für Coil-Setups
- Ladefunktion für Coil-Setups


## [0.2.1] - 2017-06-12
### Changed
- Daten für V4A korrigiert


## [0.0.1] - 2017-06-03
### Added

- Daten für Kanthal A1
- Daten für NiChrome 80
- Daten für V4A
- Basisfunktionalität

