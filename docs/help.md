# Keulkulator



[TOC]



## Über Keulkulator

Keulkulator ist ein Programm zur Berechnung von Wicklungen für Selbstwickelverdampfer. Es gibt dafür zwar schon einige sehr gute Online-Tools und Apps für Smartphones, aber noch kein Programm zur lokalen Installation auf dem Computer. Um von einer Internet-Verbindung und dem Angebot eines Webtools unabhängig zu sein,, ist es mit dem Keulkulator möglich, ein Programm mit dieser Finktionalität auf dem eigenen Rechner vorzuhalten.



## Features

- Auswahl verschiedener Drahtmaterialien
- Freie Auswahl der Drahtstärke in mm oder AWG
- Auswahl des zu erzielenden Wicklungswiderstands
- Auswahl des Innendurchmessers der Wicklung
- Auswahl des "Befestigungswegs" zwischen Wicklung uns Anschluss-Post
- Wahlweise singlecoil, dualcoil oder quadcoil Wicklung
- Anzeige der erforderlichen Drahtlänge
- Anzeige der exakten Wicklungszahl
- Anzeige verschiedenen ganz- bzw. halbzahliger Wicklungen in Annäherung zum gewünschten Widerstand
- Anzeige von Stromstärke, Leistung und Heat flux bei einer einstellbaren Versorgungsspannung
- Materialeditor zur Erfassung eigener Drahtsorten und spezifischer Widerstände
- Speichern und Laden eine Setups



## Auswahl der Drahtsorte

Die Drahtsorte kann im Auswahlfeld aus den definierten Daten ausgewählt werden. Bei einer Änderung in diesem Feld, erfolt eine sofortige Neuberechnung aller Werte.



## Auswahl der Drahtstärke

Mit den Eingabefeldern (mit Spin-Buttons) kann die Drahtstärke in 0.01 mm Schritten bzw. in ganzzahligen AWG Schritten ausgewählt werden. Die Umrechnung zwischen Millimeter und AWG erfolgt automatisch. Wenn mit dem Schiebeschalter als Drahtmaterial "Flachdraht" ausgewählt wurde, können Breite und Stärke in 0,01 mm Schritten ausgewählt werden.



## Auswahl des Zielwiderstands

Mit dem Eingabefeld (mit Spin-Buttons) für den gewünschten Widerstand kann der Zielwiderstand der Wicklung in 0.02 mm Schrittenausgewählt werden. 



## Auswahl des Innendurchmessers der Wicklung

Mit dem Eingabefeld (mit Spin-Buttons) für den Innendurchmesser der Wicklung kann dieser Wert im Bereich von 0.5 bis 5.0 mm in 0.5 mm Schritten ausgewählt werden.



## Auswahl Befestigungsweg

Mit dem Eingabefeld (mit Spin-Buttons) für den Befestigungsweg kann der Abstand zwischen eigentlicher Wicklung und Befestigungspost des Wickeldecks im Bereich von 0.00 bis 10.0 mm in 0.5 mm Schritten ausgewählt werden.

Hinweis: Hier wird der "einfache Weg", also der Abstand zwischen einem Post und der Wicklung eingegeben.



## Singlecoil, Dualcoil, Quadcoil

In diesem Auswahlfeld wird die Anzahl der Wicklungen auf dem Wickeldeck ausgewählt (eine, zwei oder vier Wicklungen). In der Ausgabe wird dann der erforderliche Wert für jeweils eine der Wicklungen ausgegeben.



## Akkuspannung

Die Anzeige von Strom und Leistung erfolgt in Abhängigkeit einer im Eingabefeld (mit Spin-Buttons) ausgewählten Akkuspannung zum Betrieb der Wicklung. Bei hohen Spannungen bzw. niedrigem Wicklungswiderstand ist besonderes Augenmerk aif die Angabe der Stromstärke zu legen. Der Akku muss mit entsprechender Stromstärke (A) belastbar sein, ansonsten besteht die Gefahr schneller Alterung oder gar einer Beschädigung der Akkuzelle, die bis zur Ausgasung führen kann.



## Setup speichern…

Das "Setup" einer Wicklung, also die Auswahl von Material, Drahtstärke, Zielwiderstand, Wicklungsdurchmesser, Befestigungsweg und Coil-Anzahl kann gespeichert werden. Nach dem Speichern wird der Name des Steups (Dateiname) im Feld "Setup" angezeigt.



## Setup laden…

Ein gespeichertes "Setup" einer Wicklung, also die Auswahl von Material, Drahtstärke, Zielwiderstand, Wicklungsdurchmesser, Befestigungsweg und Coil-Anzahl kann geladen werden. Nach dem Laden wird der Name des Steups (Dateiname) im Feld "Setup" angezeigt.



## Material-Editor

Mit dem Material-Editor (Button neben dem Material-Auswahlfeld) können Parameter der vorliegenden Daten angepasst werden und auch neue Materialien mit dem jeweiligen spezifischen Widerstand zur Liste hinzugefügt werden.

Die Parameter eines in der Liste ausgewählten Materials können geändert werden. Die Änderungen werden mit dem Button "Übernehmen" übernommen.

Der Button "Felder löschen" löscht die Materialbezeichnung und setzt den spezifischen Widerstand zurück (0.01 Ohm/m). Es können neue Materialien eingegeben werden. Mit dem Button "Eintrag zufügen" wird das neue Material zur Liste hinzugefügt.

Der Button "OK" übernimmt die durchgeführten Änderungen und es wird ins Hauptprogramm zurückgekehrt. Achtung: Wird das Programm beendet, verfallen die Änderungen, sofern nicht vor Verlassen des Programms im Material-Editor mit dem Button "Speichern" die aktuelle Materialliste in der Konfigurationsdatei gespeichert wurde. Der Button "Speichern" speichert die aktuelle Liste und springt ins Hauptprogramm zurück.

Ein Klick auf den Button "Verwerfen" setzt die Materialliste auf den Stand vor den Änderungen zurück, sofern die geänderte Materialliste noch nicht gespeichert wurde.



## FAQ

1. **Warum steht "meine" Drahtsorte nicht zur Auswahl?**

   Es wurden die gebräuchlichsten Materialien als Vorauswahl erfasst. Mit dem Material-Editor ist es ganz einfach möglich, eigene Einträge für weitere Materialien vorzunehmen und dauerhaft zu speichern.

2. **Wenn ich eine Berechnung mit einem anderen Dampfspulenrechner durchführe, erhalte ich abweichende Werte. Wie kommt das?**

   Die Genauigkeit der Berechnungen hängt von der internen genauigkeit für Rechenoperationen und Variablen ab. Keulkulator rechnet intern mit dem Datentyp Float, der in Abhängigkeit von der verwendeten Hardware mit 8 bzw. 10 Byte umfasst. Geringe Abweichungen sind damit durchaus möglich, spielen in der Praxis aber keine Rolle. Einige Wickelrechner arbeiten auch mit gerundeten oder abweichenden Werten für den spezifischen Widerstand bzw. Widerstand pro Meter, was Abweichungen ebenfalls erklärt. Die Werte des Keulkulator sind aber auf jeden Fall sehr genau und praxistauglich.

3. **Wieso werden drei verschiedene Ergebnisse angezeigt? Welches ist für mich das Richtige?**

   Aus dem eingegebenen Wunschwiderstand wird die exakte Drahtlänge berechnet. Da der Innendurchmesser der Wicklung jedoch fix ist, lässt sich die gesamte Drahtlänge nur in Ausnahmefällen komplett um die Wickelhilfe wickeln. Dir für ganze bzw. halbe Wicklungen benötigte Drahtlänge weicht in der Regel von der genauen Drahtlänge ab. Aus diesem Grund werden drei Varianten (auch in Abhängigkeit von der Befestigungsmöglichkeit auf dem Wickeldeck) angezeigt, deren Widerstand möglichst nahe bei dem gewünschten Widerstand liegt. Nun muss jeder selbst entscheiden, für welche Variante er sich entscheidet. Hinter den Ergebnissen wird auch der mit der jeweiligen Wicklung erreichte Widerstand angegeben.

4. **Wozu dient die Anzeige von Stromstärke, Leistung und Heat flux?**

   Insbesondere für das ungeregelte Dampfen, aber auch für das Dampfen mit variabler Spannung oder variabler Leistung, ist die Angabe der Belastung bzw. der Leistung interessant. Hier kann man dann auch ablesen, ob die verwendete Akkuzelle oder der verwendete Akkuträger in der Lage ist, die Wicklung zu befeuern. Mit dem Heat flux kann man die Leistung der Wicklung n Abhängigkeit der Materialmenge des Wickeldrahtes abschätzen. Hiervon hängen Temeoratur des Dampfes und Dampfleistung ab.



## Installation

Keulkulator liegt als direkt ausfühbare Datei vor und muss nicht extra installiert werden. Das Programmverzeichnis einfach in das gewünschte Zielverzeichnis kopieren und Keulkulator direkt ausführen.

Das Programm steht auch als gepacktes Programm-Release (Windows-Version .ZIP - Linux-Version .tar.bz2) zur Verfügung.



## Erstellen

Um die aktuelle Version zu erstellen, mittels

`git clone https://github.com/PepeCyB/keulkulator.git`

klonen oder als ZIP herunterladen und entpacken. Im Programmverzeichnis "keulkulator" befindet sich die Lazarus-Projektdatei "keulkulator.lpi". Das Programm dann mit Lazarus wie gewohnt erstellen.



## Sicherheit

Die Benutzung von Litihum-Ionen-Akkus birgt gewisse Risiken. Bitte informieren Sie sich über Akkusicherheit und die maximale Belastbarkeit der Akkuzellen, bevor Sie die errechnete Wicklung in Betrieb nehmen.

Nachdem Sie eine Spule gewickelt haben, messen Sie immer den Widerstand, bevor Sie die Wicklung betreiben. Damit können sie Fehler im Setup, aber auch Fehlerquellen wie Kurzschlüsse etc. feststellen und Schäden an der Hardware bzw. Unfälle vermeiden.

Es ist sinnvoll, sich, bevor man mit dem Selbstwickeln beginnt, mit dem Ohm'schen Gesetz zu befassen und über die physikalischen Leistungsgrenzen von Akkuzellen im Allgemeinen und die Belastbarkeit der eigenen Akkus zu informieren.



## Haftungsausschluss

Keulkulator wurde von mir sehr gewissenhaft entwickelt und ich war bemüht, möglichst sämtliche Fehlerquellen auszuschließen. Trotzdem kann ich nicht garantieren, dass das Programm frei von Mängeln ist. Keulkulator wird „wie er ist“ angeboten, die Benutzung erfolgt auf eigene Gefahr. Ich gebe keine Garantie in Bezug auf Genauigkeit, Korrektheit der Berechnungen oder Eignung für einen bestimmten Zweck, weder ausdrücklich, noch implizit.

Unter keinen Umständen hafte ich für direkte, indirekte, spezielle, zufällige oder sonstige Folgeschäden, die aus der Nutzung, dem Missbrauch oder Unachtsamkeit entstehen.

Die Benutzung des Keulkulator erfolgt auf persönliche Verantwortung. Überprüfen Sie alle Ergebnisse, wenn nötig und verwenden Sie das Programm auf eigene Gefahr.



## Copyright und Lizenz

Copyright © 2017: Daniel "PepeCyB" Hagemeister-Biernath

Keulkulator ist lizensiert unter der [GNU General Public License Version 3](https://www.gnu.org/licenses/gpl.txt).

[GNU GPL 3](gpl.txt)

