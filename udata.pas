unit UData;

{$mode objfpc}{$H+}

{
This file is part of Keulkulator.

Keulkulator is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Keulkulator is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Keulkulator.  If not, see <http://www.gnu.org/licenses/>.
}

interface

uses
  Classes, SysUtils, math;
  function MyStrToFloat(s: String): Double;
  procedure InitData;


var
  configdat : TStringList;	// Daten der Konfigurationsdatei
  materialien : TStringList;	// Stringliste für Materialnamen
  spezwid : array of Float;	// dynamisches Array für spez. Widerstände
  nummat : Int64;		// Anzahl der Materialeinträge
  i : Integer;

implementation

function MyStrToFloat(s: String): Double;  // eigene Umwandlungsfunktion, um Systemunterschiede
var                                        // bezüglich des Dezimaltrenners aufzufangen
  fs: TFormatSettings;
begin
  fs := DefaultFormatSettings;
  if not TryStrToFloat(s, Result, fs) then
  begin
    if fs.DecimalSeparator = '.' then
      fs.DecimalSeparator := ','
    else
      fs.DecimalSeparator := '.';
    Result := StrToFloat(s, fs);
  end;
end;

procedure InitData;                // Draht-Werte initialisieren

begin
  configdat := TStringList.create;			// Daten laden
  configdat.LoadFromFile('material.dat');

  nummat := trunc(configdat.Count / 2);			// Double Count -> Int64

  materialien := TStringList.Create;                    // Materialien-Liste initialisieren
  SetLength(spezwid, nummat);                           // Array initialisieren

  for i := 0 to nummat-1 do
  begin
    materialien.Add(configdat[i*2]);			// Durchlauf in 2er-Schritten
    spezwid[i] := MyStrToFloat(configdat[i*2+1]);
  end;

end;

end.

